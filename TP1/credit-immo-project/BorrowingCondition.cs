﻿using System;
namespace credit_immo_project
{
    public class BorrowingCondition
    {
        public const int MinCapitalBorrowing = 50000;

        public int BorrowingCapital { get; }

        public int BorrowingDuration { get; }

        public BorrowingCondition(int borrowedCapital, int borrowingDuration)
        {
            if (borrowedCapital < MinCapitalBorrowing)
            {
                throw new Exception($"Capital borrowing can't be less than {MinCapitalBorrowing}");
            }

            BorrowingCapital = borrowedCapital;
            BorrowingDuration = ConverteYearInMonth(borrowingDuration);
        }

        private int ConverteYearInMonth(int borrowingDuration)
        {
            return borrowingDuration * 12;
        }
    }
}
