﻿using System;
namespace credit_immo_project
{
    public class Credit
    {
        private const int Pourcent = 100;
        private const int MonthInAYear= 12;

        public BorrowingCondition BorrowingCondition { get; }
        public Interest Interest { get; }
        public Insurance Insurance { get; }

        public Credit(BorrowingCondition borrowingCondition, Interest interest, Insurance insurance)
        {
            BorrowingCondition = borrowingCondition;
            Interest = interest;
            Insurance = insurance;
        }

        public double MonthlyPaymentWithoutInsurance()
        {
            return (int)Math.Round(BorrowingCondition.BorrowingCapital * ((Interest.AnnualRate / Pourcent) / MonthInAYear) / (1 - Math.Pow(1 + (Interest.AnnualRate / Pourcent) / MonthInAYear, -this.BorrowingCondition.BorrowingDuration)));
        }

        public double MonthlyPaymentWithInsurance()
        {
            return Math.Round(MonthlyPaymentWithoutInsurance() + Insurance.MonthlyInsurancePrice());
        }

        public double TotalAmountOfInterest()
        {
            return Math.Round(MonthlyPaymentWithInsurance() * BorrowingCondition.BorrowingDuration - BorrowingCondition.BorrowingCapital);
        }

        public int CapitalAmountPaidForYears(int years)
        {
            if (years <= 0 || years > BorrowingCondition.BorrowingDuration)
            {
                throw new Exception($"Years must be between 1 and {BorrowingCondition.BorrowingDuration}");
            }
            return (int)Math.Round(MonthlyPaymentWithInsurance() * MonthInAYear * years);
        }

    }
}
