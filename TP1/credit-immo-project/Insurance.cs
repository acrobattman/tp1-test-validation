﻿using System;
using System.Collections.Generic;

namespace credit_immo_project
{
    public class Insurance
    {
        private const double BasicRate = 0.3;

        private Dictionary<ELoanValueInsurance, double> insuranceRateMap = new Dictionary<ELoanValueInsurance, double> {
            { ELoanValueInsurance.Sportsman, -0.05 },
            { ELoanValueInsurance.Smoker, 0.15 },
            { ELoanValueInsurance.WithHeartProblems, 0.3 },
            { ELoanValueInsurance.WorksAsAComputerEngineer, -0.05 },
            { ELoanValueInsurance.WorksAsAFighterPilot, 0.15 }
        };

        public BorrowingCondition BorrowingCondition { get; }

        public double InsuranceRate { get; }

        public Insurance(BorrowingCondition borrowingCondition, ELoanValueInsurance[] insuranceFactor)
        {
            BorrowingCondition = borrowingCondition;
            InsuranceRate = CalculateInsuranceRate(insuranceFactor);
        }

        public double MonthlyInsurancePrice()
        {
            return Math.Round((InsuranceRate / 100) * BorrowingCondition.BorrowingCapital / 12);
        }

        public double TotalInsurancePrice()
        {
            return Math.Round(MonthlyInsurancePrice() * BorrowingCondition.BorrowingDuration);
        }

        private double CalculateInsuranceRate(ELoanValueInsurance[] insuranceFactor)
        {
            double insuranceRate = BasicRate;

            foreach (ELoanValueInsurance element in insuranceFactor)
            {
                insuranceRate += insuranceRateMap[element];
            }

            return insuranceRate;
        }
    }
}
