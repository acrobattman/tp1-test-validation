﻿using System;
using System.Collections.Generic;

namespace credit_immo_project
{
    public class Interest
    {
        private Dictionary<(int, ENominalRateQuality), double> nominalRateValues = new Dictionary<(int, ENominalRateQuality), double>
        {
            { (7, ENominalRateQuality.GoodRate), 0.62},
            { (7, ENominalRateQuality.VeryGoodRate), 0.43},
            { (7, ENominalRateQuality.ExcellentRate), 0.35},

            { (10, ENominalRateQuality.GoodRate), 0.67},
            { (10, ENominalRateQuality.VeryGoodRate), 0.55},
            { (10, ENominalRateQuality.ExcellentRate), 0.45},

            { (15, ENominalRateQuality.GoodRate), 0.85},
            { (15, ENominalRateQuality.VeryGoodRate), 0.73},
            { (15, ENominalRateQuality.ExcellentRate), 0.58},

            { (20, ENominalRateQuality.GoodRate), 1.04},
            { (20, ENominalRateQuality.VeryGoodRate), 0.91},
            { (20, ENominalRateQuality.ExcellentRate), 0.73},

            { (25, ENominalRateQuality.GoodRate), 1.27},
            { (25, ENominalRateQuality.VeryGoodRate), 1.15},
            { (25, ENominalRateQuality.ExcellentRate), 0.89},
        };

        public double AnnualRate { get; }

        public Interest((int, ENominalRateQuality) nominalRate)
        {
            AnnualRate = nominalRateValues[nominalRate];
        }
    }
}
