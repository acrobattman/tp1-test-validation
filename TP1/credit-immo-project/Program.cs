﻿using System;

namespace credit_immo_project
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            var insuranceTerm = new ELoanValueInsurance[] { ELoanValueInsurance.Smoker, ELoanValueInsurance.WorksAsAComputerEngineer, ELoanValueInsurance.WithHeartProblems };

            var borrowingCondition = new BorrowingCondition(175000, 25);

            var insurance = new Insurance(borrowingCondition, insuranceTerm);

            var interest = new Interest((25, ENominalRateQuality.GoodRate));

            Credit credit = new Credit(borrowingCondition, interest, insurance);

            Console.WriteLine("Question 1 : Quel est le prix global de la mensualité ?");
            Console.WriteLine(credit.MonthlyPaymentWithInsurance());
            Console.WriteLine("Question 2 : Quel est le montant de la cotisation mensuelle d’assurance ?");
            Console.WriteLine(insurance.MonthlyInsurancePrice());
            Console.WriteLine("Question 3 : Quel est le montant total des intérêts remboursés ?");
            Console.WriteLine(credit.TotalAmountOfInterest());
            Console.WriteLine("Question 4 : Quel est le montant total de l’assurance ?");
            Console.WriteLine(insurance.TotalInsurancePrice());
            Console.WriteLine("Question 5 : Quel montant du capital a été remboursé au bout de 10 ans ?");
            Console.WriteLine(credit.CapitalAmountPaidForYears(10));

            Console.WriteLine("\n");


            var insuranceTerm1 = new ELoanValueInsurance[] { ELoanValueInsurance.Sportsman, ELoanValueInsurance.WorksAsAFighterPilot };

            var borrowingCondition1 = new BorrowingCondition(200000, 15);

            var insurance1 = new Insurance(borrowingCondition1, insuranceTerm1);

            var interest1 = new Interest((15, ENominalRateQuality.VeryGoodRate));

            Credit credit1 = new Credit(borrowingCondition1, interest1, insurance1);

            Console.WriteLine("Question 1 : Quel est le prix global de la mensualité ?");
            Console.WriteLine(credit1.MonthlyPaymentWithInsurance());
            Console.WriteLine("Question 2 : Quel est le montant de la cotisation mensuelle d’assurance ?");
            Console.WriteLine(insurance1.MonthlyInsurancePrice());
            Console.WriteLine("Question 3 : Quel est le montant total des intérêts remboursés ?");
            Console.WriteLine(credit1.TotalAmountOfInterest());
            Console.WriteLine("Question 4 : Quel est le montant total de l’assurance ?");
            Console.WriteLine(insurance1.TotalInsurancePrice());
            Console.WriteLine("Question 5 : Quel montant du capital a été remboursé au bout de 10 ans ?");
            Console.WriteLine(credit1.CapitalAmountPaidForYears(10));
        }
    }
}
