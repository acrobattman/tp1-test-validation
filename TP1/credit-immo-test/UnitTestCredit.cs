﻿using System;
using credit_immo_project;
using Xunit;

namespace credit_immo_test
{
    public class UnitTestCredit
    {
        private static readonly ELoanValueInsurance[] insuranceTerm = new ELoanValueInsurance[] { ELoanValueInsurance.Smoker, ELoanValueInsurance.WorksAsAComputerEngineer, ELoanValueInsurance.WithHeartProblems };

        private static readonly BorrowingCondition borrowingCondition = new BorrowingCondition(175000, 25);
        private static readonly Interest interest = new Interest((25, ENominalRateQuality.GoodRate));
        private static readonly Insurance insurance = new Insurance(borrowingCondition, insuranceTerm);

        private static readonly Credit credit = new Credit(borrowingCondition, interest, insurance);

        [Fact]
        public void TestMonthlyPaymentWithInsurance()
        {
            Assert.Equal(783, credit.MonthlyPaymentWithInsurance());
        }

        [Fact]
        public void TestMonthlyInsurancePrice()
        {
            Assert.Equal(102, insurance.MonthlyInsurancePrice());
        }

        [Fact]
        public void TestTotalInsurancePrice()
        {
            Assert.Equal(30625, insurance.TotalInsurancePrice());
        }

        [Fact]
        public void TestTotalAmountOfInterest()
        {
            Assert.Equal(59966, credit.TotalAmountOfInterest()); 
        }

        [Fact]
        public void TestCapitalAmountPaidForYears()
        {
            Assert.Equal(93960, credit.CapitalAmountPaidForYears(10));
        }

    }
}