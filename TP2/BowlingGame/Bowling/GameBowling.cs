﻿using System;
using System.Collections.Generic;

namespace Bowling
{
    public class GameBowling
    {
        private int score;
        private int[] throws = new int[21];
        private int currentThrow;


        public GameBowling()
        {
        }

        public void Throw(int pins)
        {
            score += pins;
            throws[currentThrow++] = pins;
        }

        public int Score()
        {
            int score = 0;
            int throwIndex = 0;
            for(int turnToPlay = 0; turnToPlay < 10; turnToPlay++)
            {
                if(IsStrike(throwIndex))
                {
                    score += 10 + StrikeBonus(throwIndex);
                    throwIndex++;
                }
                else if(IsSpare(throwIndex))
                {
                    score += 10 + SpareBonus(throwIndex);
                    throwIndex += 2;
                }
                else
                {
                    score += SumOfPinsInTurn(throwIndex);
                    throwIndex += 2;
                }
            }

            return score;
        }

        private bool IsSpare(int throwIndex)
        {
            return throws[throwIndex] + throws[throwIndex + 1] == 10;
        }

        private bool IsStrike(int throwIndex)
        {
            return throws[throwIndex] == 10;
        }

        private int SumOfPinsInTurn(int throwIndex)
        {
            return throws[throwIndex] + throws[throwIndex + 1]; 
        }

        private int SpareBonus(int throwIndex)
        {
            return throws[throwIndex + 2];
        }

        private int StrikeBonus(int throwIndex)
        {
            return throws[throwIndex + 1] + throws[throwIndex + 2];
        }
    }
}
