﻿using System;

namespace TP2FizzBuzz
{
    class Program
    {
        static void Main(string[] args)
        {
            FizzBuzz fizzBuzz = new();
            Console.WriteLine(fizzBuzz.Generate(1, 100));
        }
    }
}
