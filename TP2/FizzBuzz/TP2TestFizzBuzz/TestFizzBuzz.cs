﻿using System;
using TP2FizzBuzz;
using Xunit;

namespace TP2TestFizzBuzz
{
    public class TestFizzBuzz
    {
        FizzBuzz fizzBuzz = new FizzBuzz();

        [Theory]
        [InlineData("1", 1, 1)]
        [InlineData("2", 2, 2)]
        [InlineData("Fizz", 3, 3)]
        [InlineData("Fizz", 6, 6)]
        [InlineData("Buzz", 5, 5)]
        [InlineData("Buzz", 10, 10)]
        [InlineData("FizzBuzz", 15, 15)]
        [InlineData("FizzBuzz", 30, 30)]
        [InlineData("12", 1, 2)]
        [InlineData("12Fizz", 1, 3)]
        [InlineData("12Fizz4Buzz", 1, 5)]
        [InlineData("12Fizz4BuzzFizz78FizzBuzz11Fizz1314FizzBuzz", 1, 15)]
        public void FizzBuzzTest(string expectedResult, int minNumber, int maxNumber)
        {
            Assert.Equal(expectedResult, fizzBuzz.Generate(minNumber, maxNumber));
        }
    }
}
